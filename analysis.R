# data set loading
data <- read.csv(file.choose(),header=T)
View(data)

# Assessing the missing values
sapply(data, function(x)(sum(is.na(x)))) 
# Names of attributes 
names(data)
# Data set dimension
dim(data)   
# Description of data
summary(data)
# Changing names of variables
names(data)[c(3)] <- c("parent_level_edu") 
View(data)
names(data)[c(5)] <- c("test_prep_course")
View(data)
# Column data id
id <- rownames(data)
data <- cbind(id=id, data)
View(data)
# Description of updataed data
dim(data)
summary(data$math.score)
summary(data$reading.score)
summary(data$writing.score)
summary(data$gender)
summary(data$race.ethnicity)
summary(data$parent_level_edu)
summary(data$lunch)
summary(data$test_prep_course)

# Box plot and other visuals
boxplot(math.score) 
mean(math.score)
boxplot(reading.score)
mean(reading.score)
boxplot(writing.score)
mean(writing.score)
hist(math.score)          
library(tidyverse)
library(ggplot2)
# Implementing t-test mathematics
y<- math.score[gender=="male"]   
x<- math.score[gender=="female"]  
boxplot(math.score~gender,data=data, col=rainbow(3), xlab="Gender", ylab="Math Score")   
t.test(y,x, alternative = "less")   
# Implementing t-test reading
yy <- reading.score[gender=="male"]  
xx <- reading.score[gender=="female"]  
boxplot(reading.score~gender,data=data, col=rainbow(9), xlab="Gender", ylab="Reading Score") 
t.test(yy,xx, alternative = "less")  
# Implementing t-test writing
yyy <- writing.score[gender=="male"]  #male
xxx <- writing.score[gender=="female"]  #female
boxplot(writing.score~gender,data=data, col=rainbow(9), xlab="Gender", ylab="Writing Score") 
t.test(yyy,xxx, alternative = "less")   
table(plot_data$score)
ggplot(data = plot_data, aes(x =lunch)) + geom_histogram(bins = 100, color = 'green')

# Anova
mutate(data, score=rowMeans(data[,7:9])) -> data_new  
View(data_new)
x <- subset(data_new, (parent_level_edu=="bachelor's degree" | parent_level_edu=="some college"  
                       | parent_level_edu=="master's degree" | parent_level_edu=="associate's degree" 
                       | parent_level_edu=="high school"  | parent_level_edu=="some high school") )  
model<-aov(data_new$score~data_new$parent_level_edu,data = x)
summary(model)  
TukeyHSD(model)
ggplot(data=data_new, aes(y=score, x= parent_level_edu))+geom_boxplot(aes(fill=parent_level_edu))+theme(axis.text.x = element_text(angle = 90, hjust = 1))+ggtitle("Average Score V/S Parent Education")+xlab("Parent Education Level")+ylab("Average Score")
# Correlation of Scores
ggplot(data, aes(x = math.score, y = reading.score)) + geom_point()    
ggplot(data, aes(x = writing.score, y = reading.score)) + geom_point() 
